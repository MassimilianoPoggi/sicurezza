#ifndef SICUREZZA_ARP_SPOOFER
#define SICUREZZA_ARP_SPOOFER

#include <net/ethernet.h> //ether_addr
#include <netinet/in.h> //in_addr


struct ifinfo {
	int ifindex;
	struct ether_addr mac;
	struct in_addr ip;
};

struct ifinfo *find_interface_info(int s, char *ifname);
struct ether_addr *find_target_mac(int sck, struct ifinfo *ifr, struct in_addr *target);
int spoof(struct ifinfo *ifr, struct in_addr *spoof_source, struct in_addr *spoof_destination);
#endif
